import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import API from './API';
import json from './data';

class App extends React.Component {
    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h2>Welcome to React</h2>
                </div>
                <div className= "serverBoxesContainers">
                    <API />
                </div>
            </div>
        );
    }
}
export default App;


function WriteLastResults(TestNumber, result) {
    var obj = json;
    var currentDate = new Date();
    if (TestNumber === 1) {
        obj.Test1.id = TestNumber;
        obj.Test1.date = String(currentDate);
        obj.Test1.status = result;
    }
    else if (TestNumber === 2) {
        obj.Test1.id = TestNumber;
        obj.Test1.date = String(currentDate);
        obj.Test1.status = result;
    }
    else if (TestNumber === 3) {
        obj.Test1.id = TestNumber;
        obj.Test1.date = String(currentDate);
        obj.Test1.status = result;
    }
    else if (TestNumber === 4) {
        obj.Test1.id = TestNumber;
        obj.Test1.date = String(currentDate);
        obj.Test1.status = result;
    }
    else if (TestNumber === 5) {
        obj.Test1.id = TestNumber;
        obj.Test1.date = String(currentDate);
        obj.Test1.status = result;
    }
}

function GetLastResults(TestNumber) {
    var obj = json;
    var entry;
    if (TestNumber === 1) {
        entry = obj.Test1
    }
    else if (TestNumber === 2) {
        entry = obj.Test1
    }
    else if (TestNumber === 3) {
        entry = obj.Test1
    }
    else if (TestNumber === 4) {
        entry = obj.Test1
    }
    else if (TestNumber === 5) {
        entry = obj.Test1
    }

    return entry;
}