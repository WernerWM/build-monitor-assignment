import React from "react";
import './App.css';
var xhttp = new XMLHttpRequest();
var button1Clicked = 1;
var button2Clicked = 1;
var button3Clicked = 1;
var button4Clicked = 1;
var button5Clicked = 1;

class EndpointOne extends React.Component{

    constructor(props){
        super(props);
        this.handlerEndpointTestOneClick = this.handlerEndpointTestOneClick.bind(this);
        this.state = {
            endpointBlockClicked : 0
        }
    }

    handlerEndpointTestOneClick() {
        button1Clicked += 1;
        if (button1Clicked % 2 === 0) {
            this.setState({endpointBlockClicked: 1});
        }
        else{
            this.setState({endpointBlockClicked: 0});
        }

    }

    render() {
        const endpointBlockClicked = this.state.endpointBlockClicked;
        let button1 = <Button1 status = {this.props.status === 200 ? "Green" : this.props.status === 404 ? "Red" : "Grey"} onClick={this.handlerEndpointTestOneClick}/>;
        let endpoint = "";
        if (endpointBlockClicked === 1) {
            endpoint = < EndpointOneResults response = {this.props.response}/>;
        }
        return (
            <div>
                <div className= "endpoint">
                    {button1}
                    {endpoint}
                </div>
            </div>
        )
    }
}

class EndpointOneResults extends React.Component {

    render() {
        return(
            <div>
                <div>
                    <h4>{this.props.response}</h4>
                </div>
            </div>
        )
    }
}

function Button1(props) {

    return (
        <button className={props.status}  onClick={props.onClick}>
            Button 1
        </button>
    );
}

class EndpointTwo extends React.Component{
    constructor(props){
        super(props);
        this.handlerEndpointTestTwoClick = this.handlerEndpointTestTwoClick.bind(this);
        this.state = {
            endpointBlockClicked : 0
        }
    }

    handlerEndpointTestTwoClick() {
        button2Clicked += 1;
        if (button2Clicked % 2 === 0) {
            this.setState({endpointBlockClicked: 1});
        }
        else{
            this.setState({endpointBlockClicked: 0});
        }
    }

    render() {
        const endpointBlockClicked = this.state.endpointBlockClicked;
        let button2 = <Button2 status = {this.props.status === 200 ? "Green" : this.props.status === 404 ? "Red" : "Grey"} onClick={this.handlerEndpointTestTwoClick}/>;
        let endpoint = "";
        if (endpointBlockClicked === 1) {
            endpoint = <EndpointTwoResults response={this.props.response} />
        }
        return (
            <div>
                <div className= "endpoint">
                    {button2}
                    {endpoint}
                </div>
            </div>
        )
    }
}

class EndpointTwoResults extends React.Component {

    render() {
        return(
            <div>
                <div>
                    <h4>{this.props.response}</h4>
                </div>
            </div>
        )
    }
}

function Button2(props) {
    return (
        <button className= {props.status} onClick={props.onClick}>
            Button 2
        </button>
    );
}

class EndpointThree extends React.Component{
    constructor(props){
        super(props);
        this.handlerEndpointTestThreeClick = this.handlerEndpointTestThreeClick.bind(this);
        this.state = {
            endpointBlockClicked : 0
        }
    }

    handlerEndpointTestThreeClick() {
        button3Clicked += 1;
        if (button3Clicked % 2 === 0) {
            this.setState({endpointBlockClicked: 1});
        }
        else{
            this.setState({endpointBlockClicked: 0});
        }
    }

    render() {
        const endpointBlockClicked = this.state.endpointBlockClicked;
        let button3 = <Button3 status = {this.props.status === 200 ? "Green" : this.props.status === 404 ? "Red" : "Grey"} onClick={this.handlerEndpointTestThreeClick}/>;
        let endpoint = "";
        if (endpointBlockClicked === 1) {
            endpoint = <EndpointThreeResults response ={this.props.response}/>
        }
        return (
            <div>
                <div className= "endpoint">
                    {button3}
                    {endpoint}
                </div>
            </div>
        )
    }
}

class EndpointThreeResults extends React.Component {

    render() {
        return(
            <div>
                <div>
                    <h4>{this.props.response}</h4>
                </div>
            </div>
        )
    }
}

function Button3(props) {
    return (
        <button className= {props.status} onClick={props.onClick}>
            Button 3
        </button>
    );
}

class EndpointFour extends React.Component{
    constructor(props){
        super(props);
        this.handlerEndpointTestFourClick = this.handlerEndpointTestFourClick.bind(this);
        this.state = {
            endpointBlockClicked : 0
        }
    }

    handlerEndpointTestFourClick() {
        button4Clicked += 1;
        if (button4Clicked % 2 === 0) {
            this.setState({endpointBlockClicked: 1});
        }
        else{
            this.setState({endpointBlockClicked: 0});
        }
    }

    render() {
        const endpointBlockClicked = this.state.endpointBlockClicked;
        let button4 = <Button4 status = {this.props.status === 200 ? "Green" : this.props.status === 404 ? "Red" : "Grey"} onClick={this.handlerEndpointTestFourClick}/>;
        let endpoint = "";
        if (endpointBlockClicked === 1){
            endpoint = <EndpoinFourResults response={this.props.response}/>
        }
        return (
            <div>
                <div className= "endpoint">
                    {button4}
                    {endpoint}
                </div>
            </div>
        )
    }
}

class EndpoinFourResults extends React.Component {

    render() {
        return(
            <div>
                <div>
                    <h4>{this.props.response}</h4>
                </div>
            </div>
        )
    }
}

function Button4(props) {
    return (
        <button className= {props.status} onClick={props.onClick}>
            Button 4
        </button>
    );
}

class EndpointFive extends React.Component{
    constructor(props){
        super(props);
        this.handlerEndpointTestFiveClick = this.handlerEndpointTestFiveClick.bind(this);
        this.state = {
            endpointBlockClicked : 0
        }
    }

    handlerEndpointTestFiveClick() {
        button5Clicked += 1;
        if (button5Clicked % 2 === 0) {
            this.setState({endpointBlockClicked: 1});
        }
        else{
            this.setState({endpointBlockClicked: 0});
        }
    }

    render() {
        const endpointBlockClicked = this.state.endpointBlockClicked;
        let button5 = <Button5 status = {this.props.status === 200 ? "Green" : this.props.status === 404 ? "Red" : "Grey"} onClick={this.handlerEndpointTestFiveClick}/>;
        let endpoint = "";
        if (endpointBlockClicked === 1){
            endpoint = <EndpointFiveResults response ={this.props.response}/>
        }
        return (
            <div>
                <div className= "endpoint">
                    {button5}
                    {endpoint}
                </div>
            </div>
        )
    }
}

class EndpointFiveResults extends React.Component {

    render() {
        return(
            <div>
                <div>
                    <h4>{this.props.response}</h4>
                </div>
            </div>
        )
    }
}

function Button5(props) {
    return (
        <button className= {props.status} onClick={props.onClick}>
            Button 5
        </button>
    );
}


class API extends React.Component{

    constructor(props) {
        super(props);

        this.timer = this.timer.bind(this);
        this.setTimeValue = this.setTimeValue.bind(this);
        this.state = {status1: 0,
            status2: 0,
            status3: 0,
            status4: 0,
            status5: 0,
            response1: " ",
            response2: " ",
            response3: " ",
            response4: " ",
            response5: " "};
    }

    componentDidMount() {
        this.getTest1Status("https://cognition.dev.stackworx.cloud/api/status");
        this.timer();
    }

    getTest1Status(url) {
        var context = this;
        xhttp.onreadystatechange = function() {
            if(this.readyState === xhttp.DONE) {
                if(this.status === 200) {
                    context.getTest2Status("https://ord.dev.stackworx.io/health", this.status, this.responseText);
                }
                else{
                    context.getTest2Status("https://ord.dev.stackworx.io/health", this.status, this.responseText);
                }
            }
        };
        xhttp.open("GET", url , true);
        xhttp.send();
    }

    getTest2Status(url, status1, response1) {
        var context = this;
        xhttp.onreadystatechange = function() {
            if(this.readyState === xhttp.DONE) {
                if(this.status === 200) {
                    console.log(this.status);
                    context.getTest3Status("https://api.durf.dev.stackworx.io/health", status1, this.status, response1, this.responseText);
                }
                else{
                    context.getTest3Status("https://api.durf.dev.stackworx.io/health", status1, this.status, response1, this.responseText);
                }
            }
        };
        xhttp.open("GET", url , true);
        xhttp.send();
    }

    getTest3Status(url, status1, status2, response1, response2) {
        var context = this;
        xhttp.onreadystatechange = function() {
            if(this.readyState === xhttp.DONE) {
                if(this.status === 200) {
                    context.getTest4Status("https://prima.run/health", status1, status2, this.status, response1, response2, this.responseText);
                }
                else{
                    context.getTest4Status("https://prima.run/health", status1, status2, this.status, response1, response2, this.responseText);
                }
            }
        };
        xhttp.open("GET", url , true);
        xhttp.send();
    }

    getTest4Status(url, status1, status2, status3, response1, response2, response3) {
        var context = this;
        xhttp.onreadystatechange = function() {
            if(this.readyState === xhttp.DONE) {
                if(this.status === 200) {
                    context.getTest5Status("https://stackworx.io/", status1, status2, status3, this.status, response1, response2, response3, this.responseText);
                }
                else{
                    context.getTest5Status("https://stackworx.io/", status1, status2, status3, this.status, response1, response2, response3, this.responseText);
                }
            }
        };
        xhttp.open("GET", url , true);
        xhttp.send();
    }

    getTest5Status(url, status1, status2, status3, status4, response1, response2, response3, response4) {
        var context = this;
        xhttp.onreadystatechange = function() {
            if(this.readyState === xhttp.DONE) {
                if(this.status === 200) {
                    console.log(this.status);
                    context.setState({status1: status1,
                        status2: status2,
                        status3: status3,
                        status4: status4,
                        status5: this.status,
                        response1: response1,
                        response2: response2,
                        response3: response3,
                        response4: response4,
                        response5: this.responseText});
                }
                else{
                    context.setState({status1: status1,
                        status2: status2,
                        status3: status3,
                        status4: status4,
                        status5: this.status,
                        response1: response1,
                        response2: response2,
                        response3: response3,
                        response4: response4,
                        response5: this.responseText});
                }
            }
        };
        xhttp.open("GET", url , true);
        xhttp.send();
    }

    timer() {
        console.log('hello');
        setInterval(this.setTimeValue, 300000)
    }

    setTimeValue() {
        console.log("Got you");
        this.getTest1Status("https://cognition.dev.stackworx.cloud/api/status");

    }

    render() {

        return (
            <div>
                < EndpointOne status = {this.state.status1} response = {this.state.response1}/>
                < EndpointTwo status = {this.state.status2} response = {this.state.response2}/>
                < EndpointThree status = {this.state.status3} response = {this.state.response3}/>
                < EndpointFour status = {this.state.status4} response = {this.state.response4}/>
                < EndpointFive status = {this.state.status5} response = {this.state.response5}/>
            </div>
        );
    }
}

export default API;
